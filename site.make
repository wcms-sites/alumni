core = 7.x
api = 2

; token_data_hash
projects[token_data_hash][type] = "module"
projects[token_data_hash][download][type] = "git"
projects[token_data_hash][download][url] = "https://git.uwaterloo.ca/drupal-org/token_data_hash.git"
projects[token_data_hash][download][tag] = "7.x-1.1"
projects[token_data_hash][subdir] = ""

; variablecheck
projects[variablecheck][type] = "module"
projects[variablecheck][download][type] = "git"
projects[variablecheck][download][url] = "https://git.uwaterloo.ca/drupal-org/variablecheck.git"
projects[variablecheck][download][tag] = "7.x-1.4"
projects[variablecheck][subdir] = ""

; Views Datasource
projects[views_datasource][type] = "module"
projects[views_datasource][download][type] = "git"
projects[views_datasource][download][url] = "https://git.uwaterloo.ca/drupal-org/views_datasource.git"
projects[views_datasource][download][tag] = "7.x-1.0-alpha2"
projects[views_datasource][subdir] = ""

; uw_alumni_map
projects[uw_alumni_map][type] = "module"
projects[uw_alumni_map][download][type] = "git"
projects[uw_alumni_map][download][url] = "https://git.uwaterloo.ca/wcms/uw_alumni_map.git"
projects[uw_alumni_map][download][tag] = "7.x-1.5"
projects[uw_alumni_map][subdir] = ""

; uw_ct_alumni_publication
projects[uw_ct_alumni_publication][type] = "module"
projects[uw_ct_alumni_publication][download][type] = "git"
projects[uw_ct_alumni_publication][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_alumni_publication.git"
projects[uw_ct_alumni_publication][download][tag] = "7.x-1.2"
projects[uw_ct_alumni_publication][subdir] = ""


; uw_perm_webform_result_json
projects[uw_perm_webform_result_json][type] = "module"
projects[uw_perm_webform_result_json][download][type] = "git"
projects[uw_perm_webform_result_json][download][url] = "https://git.uwaterloo.ca/wcms/uw_perm_webform_result_json.git"
projects[uw_perm_webform_result_json][download][tag] = "7.x-1.1"
projects[uw_perm_webform_result_json][subdir] = ""


; uw_webform_result_json
projects[uw_webform_result_json][type] = "module"
projects[uw_webform_result_json][download][type] = "git"
projects[uw_webform_result_json][download][url] = "https://git.uwaterloo.ca/wcms/uw_webform_result_json.git"
projects[uw_webform_result_json][download][tag] = "7.x-1.2"
projects[uw_webform_result_json][subdir] = ""

